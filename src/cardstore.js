import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const CardStore = new Vuex.Store({
    state: {
        cards: [],
        cardFilter: "",
    },

    mutations: {
        updateCardFilter(state, value) {
            state.cardFilter = value;
        },
    },

    actions: {
        async getCard() {},
    },
});

export default CardStore;
