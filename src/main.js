import Vue from "vue";
// import Vuex from "vuex";
import App from "./App.vue";
import vuetify from "./plugins/vuetify";
import axios from "axios";
import store from "./cardstore.js";

Vue.prototype.$http = axios;
Vue.config.productionTip = false;

// Vue.use(Vuex);

new Vue({
    vuetify,
    store,
    render: (h) => h(App),
}).$mount("#app");
