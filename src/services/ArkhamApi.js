import axios from 'axios';

class ArkhamApi {

    constructor() {
        this.baseUrl = 'https://arkhamdb.com';
        this.cards = [];
    }

    _fetch(url) {
        const seconds = new Date().getTime();
        const fullUrl = `${this.baseUrl}/${url}?_=${seconds}`;
        return axios
            .get(fullUrl, { crossdomain: true })
            .then(res => res.data);
    }

    async getDeck(deckId) {
        const url = `api/public/deck/${deckId}.json`;
        const cards = await this.getCards();

        return this._fetch(url).then(deck => {
            deck.cards = [];

            Object.keys(deck.slots).forEach(async code => {
                let cardFromStore = cards.find(c => c.code == code);

                // Some scenario specific cards are not included to card list
                // If this happens we must query that card specially...
                if(!cardFromStore) {
                    cardFromStore = await this._fetch(`api/public/card/${code}`);
                }

                // Use original card instead of later copy
                if(cardFromStore.duplicate_of_code) {
                    console.log(`Replace ${cardFromStore.code} with ${cardFromStore.duplicate_of_code} (${cardFromStore.name})`);
                    cardFromStore = cards.find(c => c.code == cardFromStore.duplicate_of_code);                    
                }

                // If card already exists (player mixing card from dirrefent sources)
                // increase total usage
                if(deck.cards.some(card => card.code === cardFromStore.code)) {
                    const storedCard = deck.cards.find(card => card.code === cardFromStore.code);
                    storedCard.total += deck.slots[cardFromStore.code];
                }

                // Else if this is new entry just add it to a deck
                else {
                    let card = Object.assign({total: deck.slots[code]}, cardFromStore);
                    deck.cards.push(card);
                }
            });

            deck.investigatorCard = cards.find(c => c.code === deck.investigator_code);
            return deck;
        });
    }

    async getCard(code) {
        const cards = await this.getCards();
        const card = cards.find(card => card.code === code);

        return new Promise(resolve => resolve(card));
    }

    getCards() {
        if(this.cards.length > 0) {
            return new Promise(resolve => resolve(this.cards));
            // return this.cards;
        }
        
        return this._fetch('api/public/cards/')
            .then(cards => this.cards = cards);
    }
}

export default new ArkhamApi;